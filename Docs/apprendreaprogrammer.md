Introduction aux algorithmes

Sauf dans des cas très simples, on ne crée pas un programme en se lançant directement dans
l'écriture du code source. Il est d'abord nécessaire d'analyser le problème pour trouver la suite d'opérations à réaliser pour le résoudre. 
On constate qu'on arrive à l'objectif visé en combinant un ensemble d'actions dans un ordre précis.
On peut distinguer différents types d'actions :

    des actions simples
    des actions conditionnelles
    des actions qui se répètent

On peut définir un algorithme comme une suite ordonnée d'opérations permettant de résoudre un problème donné. Un algorithme décompose un problème complexe en une suite d'opérations simples.


Le rôle du programmeur

Écrire des programmes qui réalisent de manière fiable les tâches attendues est la première mission du programmeur. Un débutant arrivera vite à créer des programmes simples. La difficulté apparaît lorsque que le programme évolue et se complexifie. Il faut de l'expérience et beaucoup de pratique avant d'arriver à maîtriser cette complexité.


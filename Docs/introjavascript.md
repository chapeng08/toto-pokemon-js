Sa connaissance vous ouvrira les portes de la programmation côté navigateur Web (on parle de développement front-end), côté serveur (back-end) ou côté mobile. A l'heure actuelle, beaucoup considèrent JavaScript comme la technologie la plus importante dans le monde du développement logiciel.

 la dernière version majeure du langage, appelée ES2015 ou parfois ES6, Elle apporte des évolutions très intéressantes et est maintenant bien supportée par les principaux environnements JavaScript du marché, comme le montre cette table de compatibilité (en anglais).

Alors que ce langage a été créé en 1995 (la préhistoire de l'informatique), il a depuis bien évolué. Pour situer les choses, je dirais même qu'il a connu 3 vies :

    Dans les années 90, on parlait de DHTML (Dynamic HTML). On utilisait en fait les toutes premières versions de JavaScript pour faire des petits effets dans ses pages web : afficher une image lors d'un clic sur un bouton par exemple. C'était l'époque de Netscape et d'Internet Explorer 5.5 (mon Dieu ça me rajeunit pas !).

    Dans les années 2000, on a commencé à utiliser le langage pour créer des interfaces côté client. C'est là que des bibliothèques comme jQuery ou Mootools sont apparues. Aujourd'hui, cet usage de JavaScript est très répandu et mature. On a pris l'habitude de manipuler le DOM (Document Object Model) pour affecter ses balises HTML en JavaScript et leur faire subir toutes sortes de traitements.

    Puis, aux alentours de 2010, JavaScript est entré dans une nouvelle ère. Google a commencé à rendre le langage beaucoup plus rapide avec l'apparition du navigateur Google Chrome. Avec ce navigateur est né le moteur d'exécution V8 qui a considérablement permis d'accélérer l'exécution de code JavaScript (j'y reviendrai). Des outils comme Node.js sont ensuite apparus. Les bibliothèques dont le nom finit par .js se sont multipliées : Backbone.js, Ember.js, Meteor.js. JavaScript a l'air à nouveau cool... et semble en même temps plus compliqué qu'il n'y paraissait au premier abord.


Node.js nous permet d'utiliser le langage JavaScript sur le serveur... Il nous permet donc de faire du JavaScript en dehors du navigateur !
Node.js bénéficie de la puissance de JavaScript pour proposer une toute nouvelle façon de développer des sites web dynamiques.

Jusqu'ici, JavaScript avait toujours été utilisé du côté du client, c'est-à-dire du côté du visiteur qui navigue sur notre site. Le navigateur web du visiteur (Firefox, Chrome, IE...) exécute le code JavaScript et effectue des actions sur la page web.

On peut toujours utiliser du JavaScript côté client pour manipuler la page HTML. Ca, ça ne change pas.
Par contre, Node.js offre un environnement côté serveur qui nous permet aussi d'utiliser le langage JavaScript pour générer des pages web. En gros, il vient en remplacement de langages serveur comme PHP, Java EE, etc.

JavaScript n'est pas vraiment un langage orienté objet, il est donc très loin de Java, Ruby ou Python. Ecrire une application avec Node.js demande une gymnastique d'esprit complètement différente ! C'est un peu déroutant au début pour tout vous avouer, mais quand on commence à maîtriser cet outil, on se sent un peu comme si on venait d'avoir de nouveaux super-pouvoirs qu'on ne soupçonnait pas.


Le modèle non bloquant avec Node.js

Node.js nous évite de perdre du temps en nous permettant de faire d'autres choses en attendant que les actions longues soient terminées !


Installation de node.js sous Linux

Il vous suffit donc de rentrer les commandes suivantes :

Node.js 6 (LTS) :

curl-sL https://deb.nodesource.com/setup_6.x |sudo-Ebash- 
sudo apt install -y nodejs

Node.js 8 (current) :

curl-sL https://deb.nodesource.com/setup_8.x |sudo-Ebash- 
sudo apt install -y nodejs

Et voilà le travail ! Pour vérifier que Node est bien installé, tapez quelques commandes dans la console comme :

node -v
node

La première affiche le numéro de version de Node.js que vous avez installé.
La seconde lance l'interpréteur interactif de Node.js. Vous pouvez y taper du code JavaScript (essayez simplement de taper "1+1" pour voir). Pour sortir de l'interpréteur, faites Ctrl + D.

